package navarro.jarod;

public class Cientifico extends PersonaFisica implements IAccion{
    private String especialidad;

    public Cientifico() {
        super();
    }

    public Cientifico(String nombre, String apellido, String id, String correo, String especialidad) {
        super(nombre, apellido, id, correo);
        this.especialidad = especialidad;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    @Override
    public String caminar() {
        return "El cientifico empezo a caminar a su laboratorio";
    }

    @Override
    public String hablar() {
        return "El cientifico habló sobre su ultimo proyecto";
    }

    @Override
    public String correr() {
        return "El cientifico comenzo a correr";
    }


    @Override
    public String toString() {
        return
                "Especialidad: " + especialidad +
                        ", correo"+ correo+
                        ", nombre: " + nombre +
                        ", apellido: " + apellido +
                        ", id: " + id ;

    }
}
