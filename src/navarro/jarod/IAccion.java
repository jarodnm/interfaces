package navarro.jarod;

public interface IAccion {

    public String caminar();
    public String hablar();
    public String correr();
}
