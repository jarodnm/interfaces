package navarro.jarod;

public class Maquina {
    protected String numSerie;
    protected String descripcion;

    public Maquina() {
        super();
    }

    public Maquina(String numSerie, String descripcion) {
        this.numSerie = numSerie;
        this.descripcion = descripcion;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return
                "numSerie: " + numSerie +
                        ", descripcion: " + descripcion
                ;
    }
}
