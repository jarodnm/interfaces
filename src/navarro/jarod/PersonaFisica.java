package navarro.jarod;

public class PersonaFisica extends Persona {
    protected String correo;

    public PersonaFisica() {
    super();
    }

    public PersonaFisica(String nombre, String apellido, String id, String correo) {
        super(nombre, apellido, id);
        this.correo = correo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return
                       "Correo: " + correo +
                        ", nombre: " + nombre +
                        ", apellido: " + apellido +
                        ", id: " + id ;

    }
}
