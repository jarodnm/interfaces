package navarro.jarod;

public class Robot extends Maquina implements IAccion {
    private String tammanio;
    private String fueza;
    private String velocidad;

    public Robot() {
        super();
    }

    public Robot(String numSerie, String descripcion, String tammanio, String fueza, String velocidad) {
        super(numSerie, descripcion);
        this.tammanio = tammanio;
        this.fueza = fueza;
        this.velocidad = velocidad;
    }

    public String getTammanio() {
        return tammanio;
    }

    public void setTammanio(String tammanio) {
        this.tammanio = tammanio;
    }

    public String getFueza() {
        return fueza;
    }

    public void setFueza(String fueza) {
        this.fueza = fueza;
    }

    public String getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(String velocidad) {
        this.velocidad = velocidad;
    }

    @Override
    public String caminar() {
        return "El robot comenzó a caminar";
    }

    @Override
    public String hablar() {
        return "El robot empezó a hablar!";
    }

    @Override
    public String correr() {
        return "El robot empezó a correr como si fuera un deportista";
    }

    @Override
    public String toString() {
        return
                "tamaño: " + tammanio +
                        ", fueza: " + fueza +
                        ", velocidad: " + velocidad +
                        ", numSerie: " + numSerie +
                        ", descripcion: " + descripcion;
    }

}
